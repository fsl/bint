/*  bintoptions.h

    Mark Woolrich - FMRIB Image Analysis Group

    Copyright (C) 2002 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(BintOptions_h)
#define BintOptions_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"

namespace Bint {

class BintOptions {

 public:

  virtual ~BintOptions(){};

  Utilities::Option<bool> verbose;
  Utilities::Option<int> debuglevel;
  Utilities::Option<bool> timingon;
  Utilities::Option<bool> help;
  Utilities::Option<std::string> datafile;
  Utilities::Option<std::string> maskfile;
  Utilities::Option<std::string> logdir;
  Utilities::Option<bool> forcedir;
  Utilities::Option<std::string> inference;
  Utilities::Option<int> njumps;
  Utilities::Option<int> burnin;
  Utilities::Option<int> sampleevery;
  Utilities::Option<int> updateproposalevery;
  Utilities::Option<float> acceptancerate;
  Utilities::Option<int> seed;
  Utilities::Option<float> prec;
  Utilities::Option<bool> analmargprec;

  void parse_command_line(int argc, char** argv, Utilities::Log& logger);

 protected:

  BintOptions(const std::string& str1, const std::string& str2);
  Utilities::OptionParser options;

 private:

  BintOptions();
  const BintOptions& operator=(BintOptions&);
  BintOptions(BintOptions&);

};

 inline BintOptions::BintOptions(const std::string& str1, const std::string& str2) :
   verbose(std::string("-v,-V,--verbose"), false,
	   std::string("switch on diagnostic messages"),
	   false, Utilities::no_argument),
   debuglevel(std::string("--debug,--debuglevel"), 0,
		       std::string("set debug level"),
		       false, Utilities::requires_argument),
   timingon(std::string("--to,--timingon"), false,
		       std::string("turn timing on"),
		       false, Utilities::no_argument),
   help(std::string("-h,--help"), false,
		    std::string("display this message"),
		    false, Utilities::no_argument),
   datafile(std::string("--data,--datafile"), std::string("data"),
			  std::string("data regressor data file"),
		     true, Utilities::requires_argument),
   maskfile(std::string("--mask,--maskfile"), std::string(""),
			  std::string("mask file"),
		     true, Utilities::requires_argument),
   logdir(std::string("--ld,--logdir"), std::string("logdir"),
			  std::string("log directory (default is logdir)"),
		     false, Utilities::requires_argument),
   forcedir(std::string("--forcedir"), false,
		    std::string("Use the actual directory name given - i.e. don't add + to make a new directory"),
		    false, Utilities::no_argument),
   inference(std::string("--inf,--inference"), std::string("mcmc"),
			  std::string("inference technique: mcmc\n laplace\n (default is mcmc)"),
		     false, Utilities::requires_argument),
   njumps(std::string("--nj,--njumps"), 5000,
			  std::string("Num of jumps to be made by MCMC (default is 5000)"),
		     false, Utilities::requires_argument),
   burnin(std::string("--bi,--burnin"), 500,
			  std::string("Num of jumps at start of MCMC to be discarded (default is 500)"),
		     false, Utilities::requires_argument),
   sampleevery(std::string("--se,--sampleevery"), 1,
			  std::string("Num of jumps for each sample (MCMC) (default is 1)"),
		     false, Utilities::requires_argument),
   updateproposalevery(std::string("--upe,--updateproposalevery"), 40,
		       std::string("Num of jumps for each update to the proposal density std (MCMC) (default is 40)"),
		     false, Utilities::requires_argument),
   acceptancerate(std::string("--arate,--acceptancerate"), 0.6,
			  std::string("Acceptance rate to aim for (MCMC) (default is 0.6)"),
		     false, Utilities::requires_argument),
   seed(std::string("--seed"), 10,
      std::string("seed for pseudo random number generator"),
      false, Utilities::requires_argument),
   prec(std::string("--prec"), -1,
      std::string("value to fix error precision to (default is -1, which means error precision is not fixed)"),
	false, Utilities::requires_argument),
   analmargprec(std::string("--noamp"), true,
      std::string("turn off Analytical Marginalisation of error Precision"),
	false, Utilities::no_argument),
   options(str1,str2)
   {
     try {
       options.add(verbose);
       options.add(debuglevel);
       options.add(timingon);
       options.add(help);
       options.add(datafile);
       options.add(maskfile);
       options.add(logdir);
       options.add(forcedir);
       options.add(inference);
       options.add(njumps);
       options.add(burnin);
       options.add(sampleevery);
       options.add(updateproposalevery);
       options.add(acceptancerate);
       options.add(seed);
       options.add(prec);
       options.add(analmargprec);
     }
     catch(Utilities::X_OptionError& e) {
       options.usage();
       std::cerr << std::endl << e.what() << std::endl;
     }
     catch(std::exception &e) {
       std::cerr << e.what() << std::endl;
     }

   }
}

#endif
