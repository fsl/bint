/*  meanoptions.h

    Mark Woolrich - FMRIB Image Analysis Group

    Copyright (C) 2002 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(MeanOptions_h)
#define MeanOptions_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"
#include "bintoptions.h"

namespace Bint {

class MeanOptions : public BintOptions {
 public:
  static MeanOptions& getInstance();
  ~MeanOptions() { delete gopt; }

  Utilities::Option<float> priormean;
  Utilities::Option<float> priorstd;

 private:
  MeanOptions();
  const MeanOptions& operator=(MeanOptions&);
  MeanOptions(MeanOptions&);

  static MeanOptions* gopt;

};

 inline MeanOptions& MeanOptions::getInstance(){
   if(gopt == NULL)
     gopt = new MeanOptions();

   return *gopt;
 }

 inline MeanOptions::MeanOptions() :
   BintOptions("mean", "mean --verbose\n"),
   priormean(std::string("--pm,--priormean"), 1,
      std::string("priormean"),
	     false, Utilities::requires_argument),
   priorstd(std::string("--ps,--priorstd"), 1,
      std::string("priorstd"),
      false, Utilities::requires_argument)
   {
     try {
       options.add(priormean);
       options.add(priorstd);
     }
     catch(Utilities::X_OptionError& e) {
       options.usage();
       std::cerr << std::endl << e.what() << std::endl;
     }
     catch(std::exception &e) {
       std::cerr << e.what() << std::endl;
     }

   }
}

#endif
