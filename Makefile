include ${FSLCONFDIR}/default.mk

PROJNAME = bint
XFILES   = mean
SOFILES  = libfsl-bint.so
LIBS     = -lfsl-utils -lfsl-newimage -lfsl-miscmaths \
           -lfsl-NewNifti -lfsl-znz -lfsl-cprob
OBJS     = model.o lsmcmcmanager.o lslaplacemanager.o \
           bintoptions.o meanoptions.o

all: ${XFILES} libfsl-bint.so

libfsl-bint.so: ${OBJS}
	${CXX} ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}

mean: mean.cc libfsl-bint.so
	${CXX}  ${CXXFLAGS} -o $@ $< -lfsl-bint ${LDFLAGS}
