/*  Mean

    Mark Woolrich - FMRIB Image Analysis Group

    Copyright (C) 2002 University of Oxford  */

/*  CCOPYRIGHT  */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <math.h>
#include "utils/log.h"
#include "meanoptions.h"
#include "utils/tracer_plus.h"
#include "miscmaths/miscprob.h"
#include "stdlib.h"
#include "model.h"
#include "lsmcmcmanager.h"
#include "lslaplacemanager.h"
#include "newimage/newimageall.h"

using namespace std;
using namespace Bint;
using namespace Utilities;
using namespace NEWMAT;
using namespace MISCMATHS;
using namespace NEWIMAGE;

class MeanModel : public ForwardModel
  {
  public:

    MeanModel(int pntpts, int pdebuglevel)
      : ForwardModel(pdebuglevel),
	ntpts(pntpts)
    {}

    ~MeanModel(){}

    virtual void setparams();
    ReturnMatrix nonlinearfunc(const ColumnVector& paramvalues) const;
    void initialise(const ColumnVector& data){}
  protected:

    int ntpts;
};

void MeanModel::setparams()
{
  Tracer_Plus tr("MeanModel::setdata");

  clear_params();
  GaussPrior tmp(MeanOptions::getInstance().priormean.value(),MeanOptions::getInstance().priorstd.value());
  add_param("mean",1,0.1,tmp);
}

ReturnMatrix MeanModel::nonlinearfunc(const ColumnVector& paramvalues) const
{
  Tracer_Plus trace("MeanModel::nonlinearfunc");

  ColumnVector ret(ntpts);
  ret = paramvalues(1);
  ret.Release();

  return ret;
}

int main(int argc, char *argv[])
{
  try{

    // Setup logging:
    Log& logger = LogSingleton::getInstance();

    // parse command line - will output arguments to logfile
    MeanOptions& opts = MeanOptions::getInstance();
    opts.parse_command_line(argc, argv, logger);

    srand(MeanOptions::getInstance().seed.value());

    if(opts.debuglevel.value()==1)
      Tracer_Plus::setrunningstackon();

    if(opts.timingon.value())
      Tracer_Plus::settimingon();

    // read data
    volume4D<float> input;
    read_volume4D(input,opts.datafile.value());
    int ntpts=input.tsize();
    volume4D<float> maskNew;
    read_volume4D(maskNew,opts.maskfile.value());
    maskNew.threshold(1e-16,maskNew.max()+1,inclusive);
    Matrix data=input.matrix(maskNew[0]);

    cout << "ntpts=" << ntpts << endl;
    cout << "nvoxels=" << maskNew.sum() << endl;


    MeanModel model(ntpts,MeanOptions::getInstance().debuglevel.value());

    LSMCMCManager lsmcmc(MeanOptions::getInstance(),model,data,maskNew);
     LSLaplaceManager lslaplace(MeanOptions::getInstance(),model,data,maskNew);

     if(MeanOptions::getInstance().inference.value()=="mcmc")
       {
 	lsmcmc.setup();
 	lsmcmc.run();
 	lsmcmc.save();
       }
     else
       {
	 lslaplace.setup();
	 lslaplace.run();
	 lslaplace.save();
       }

    if(opts.timingon.value())
      Tracer_Plus::dump_times(logger.getDir());

    cout << endl << "Log directory was: " << logger.getDir() << endl;
  }
  catch(Exception& e) {
    cerr << endl << e.what() << endl;
  }
  catch(X_OptionError& e) {
    cerr << endl << e.what() << endl;
  }


  return 0;
}
