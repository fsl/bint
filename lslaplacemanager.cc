/*  LSLaplaceManager.cc

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#include "lslaplacemanager.h"
#include "utils/log.h"
#include "utils/tracer_plus.h"

using namespace std;
using namespace Utilities;
using namespace MISCMATHS;
using namespace NEWIMAGE;
using namespace NEWMAT;

// priorstd=load('stddevs');clear tmp; d=ra('data'); dataprec = 1/(1)^2; priormean=0;  for i=1:length(priorstd), priorprec = 1/(priorstd(i))^2; tmp(i) = (dataprec*sum(squeeze(d(1,1,1,:)))+priorprec*priormean)/(1000*dataprec+priorprec);end;load means;plot(priorstd,means,priorstd,tmp);legend('mcmc','theory');

namespace Bint {

  float SumSquaresEvalFunction::evaluate(const ColumnVector& x) const
  {
    Tracer_Plus tr("SumSquaresEvalFunction::evaluate");

    ntpts = data.Nrows();

    int nparams = model.getnparams();
    float energy = 1e16;

    if(!analmargprec)
	{
	  float precision;
	  if(updateprec)
	    precision = x(nparams);
	  else
	    precision = prec;

	  if(precision > 0)
      {
        energy=(data-model.nonlinearfunc(x)).SumSquare()*precision/2.0 - ntpts/2.0*std::log(precision);

        for(int p=0;p<nparams;p++)
		{
		  energy += model.getparam(p).getprior().calc_energy(x(p+1));
		}

        if(debuglevel==2)
		{
		  cout << ntpts << endl;
		  cout << (data-model.nonlinearfunc(x)).SumSquare()*precision/2.0 - ntpts/2.0*std::log(precision) << endl;

		  for(int p=0;p<nparams;p++)
          {
            cout << p << endl;
            cout << x(p+1) << endl;
            cout << model.getparam(p).getprior().calc_energy(x(p+1)) << endl;
          }

		  cout << energy << endl;
		  cout << precision << endl;
		  cout << (data-model.nonlinearfunc(x)).SumSquare() << endl;
		  cout << x << endl;
		}
      }
	}
    else
	{
	  energy = ntpts/2.0*std::log((data-model.nonlinearfunc(x)).SumSquare());

	  for(int p=0;p<nparams;p++)
      {
        energy += model.getparam(p).getprior().calc_energy(x(p+1));
      }
	}

    return energy;
  }

  float SumSquaresgEvalFunction::evaluate(const ColumnVector& x) const
  {
    Tracer_Plus tr("SumSquaresgEvalFunction::evaluate");

    ntpts = data.Nrows();

    int nparams = model.getnparams();
    float energy = 1e16;

    if(!analmargprec)
	{
	  float precision;
	  if(updateprec)
	    precision = x(nparams);
	  else
	    precision = prec;

	  if(precision > 0)
      {
        energy=(data-model.nonlinearfunc(x)).SumSquare()*precision/2.0 - ntpts/2.0*std::log(precision);

        for(int p=0;p<nparams;p++)
		{
		  energy += model.getparam(p).getprior().calc_energy(x(p+1));
		}

        if(debuglevel==2)
		{
		  cout << ntpts << endl;
		  cout << (data-model.nonlinearfunc(x)).SumSquare()*precision/2.0 - ntpts/2.0*std::log(precision) << endl;
		  for(int p=0;p<nparams;p++)
          {
            cout << p << endl;
            cout << x(p+1) << endl;
            cout << model.getparam(p).getprior().calc_energy(x(p+1)) << endl;
          }

		  cout << energy << endl;
		  cout << precision << endl;
          cout << (data-model.nonlinearfunc(x)).SumSquare() << endl;
		  cout << x << endl;
		}
      }
	}
    else
	{
	  energy = ntpts/2.0*std::log((data-model.nonlinearfunc(x)).SumSquare());

	  for(int p=0;p<nparams;p++)
      {
        energy += model.getparam(p).getprior().calc_energy(x(p+1));
      }
	}

    return energy;
  }

  ReturnMatrix SumSquaresgEvalFunction::g_evaluate(const ColumnVector& x) const
  {
    Tracer_Plus tr("SumSquaresgEvalFunction::g_evaluate");

    ntpts = data.Nrows();
    int nparams = model.getnparams();
    ColumnVector ret(x.Nrows());
    ret = 0;

    if(!analmargprec)
	{
	  float precision;
	  if(updateprec)
	    precision = x(nparams);
	  else
	    precision = prec;

	  if(precision > 0)
      {
        // need to write this bit to make laplace work with gradient info when analmargprec is turned off

        for(int p=0;p<nparams;p++)
		{

		}
        if(debuglevel==2)
		{

		}
      }
	}
    else
	{
	  float h = (data-model.nonlinearfunc(x)).SumSquare();

	  Matrix grad = model.gradient(x);

	  for(int p=1;p<=nparams;p++)
      {

        ret(p) = -ntpts*SP(data-model.nonlinearfunc(x),grad.Row(p).AsColumn()).Sum()/h + model.getparam(p-1).getprior().calc_gradient(x(p));
      }
	}

    ret.Release();
    return ret;
  }

  void LSLaplaceManager::setup()
  {
    Tracer_Plus tr("LSLaplaceManager::setup");
    ntpts = data.Nrows();
    nvoxels = data.Ncols();
  }

  void LSLaplaceManager::run()
  {
    Tracer_Plus tr("LSLaplaceManager::run");
    //      float mint = 1;
    //      float maxt = 10;

    //      ColumnVector stddevs(data.nvoxels());stddevs=0;
    //      ColumnVector means(data.nvoxels());means=0;

    for(int vox=1;vox<=data.Ncols();vox++)
    {
      cout << vox<< ",";
      cout.flush();

      if(debuglevel==2)
	  {
	    cout << endl;
	    cout << "----------------------------------" << endl;
	  }

      //  	float stddev = float((maxt-mint)*vox)/data.nvoxels()+mint;

      voxelmanager->setdata(data.Column(vox));
      voxelmanager->setupparams(precin);
      nparams = voxelmanager->getnparams();
      int nvaryingparams = voxelmanager->getnvaryingparams();
      voxelmanager->run();
      //  	stddevs(vox) = stddev;

      // use first voxel to get size of results storage
      if(vox==1)
	  {
	    covs.ReSize(nvaryingparams*nvaryingparams,nvoxels); //should this be nparams to fix error below...
	    //covs.ReSize(nparams*nparams,nvoxels);

	    covs = 0;
	    mns.ReSize(nparams,nvoxels);
	    mns = 0;

	    if(!analmargprec)
        {
          prec.ReSize(nvoxels);
          prec = 0;
        }
	  }

      mns.Column(vox) = voxelmanager->getparammeans();
      const SymmetricMatrix& symmat = voxelmanager->getparaminvcovs();

      if(!analmargprec)
        prec(vox) = voxelmanager->geterrorprecisionmean();

      ColumnVector col = MISCMATHS::reshape(symmat.i(), Sqr(symmat.Nrows()), 1).AsColumn();

      covs.Column(vox) = col; //ERROR this apppears to be broken (for certain inputs ) in the volumeseries lslaplace for noamp and still needs fixing
    }

    cout << endl;
  }

  void LSLaplaceManager::save()
  {
    Tracer_Plus tr("LSLaplaceManager::save");

    volume4D<float> output(mask);
    output.setmatrix(mns,mask[0]);

    for(int p=0;p<nparams;p++)
    {
      save_volume(output[p],LogSingleton::getInstance().appendDir(voxelmanager->getparamname(p)+string("_means")));
    }
    mns.CleanUp();

    output.setmatrix(covs,mask[0]);
    save_volume4D(output,LogSingleton::getInstance().appendDir("covs"));
    covs.CleanUp();

    if(!analmargprec)
	{
	  output.setmatrix(prec.t(),mask[0]);
	  save_volume4D(output,LogSingleton::getInstance().appendDir("prec_means"));
	  prec.CleanUp();
	}
  }

  void LSLaplaceVoxelManager::setdata(const ColumnVector& pdata)
  {
    Tracer_Plus trace("LSLaplaceVoxelManager::setdata");

    data = pdata;
    ntpts = data.Nrows();
  }

  void LSLaplaceVoxelManager::setupparams(float precin)
  {
    Tracer_Plus trace("LSLaplaceVoxelManager::setupparams");

    prec = precin;
    model.setparams();
    model.initialise(data);
    nparams = model.getnparams();

    nvaryingparams = 0;
    for(int p=0;p<nparams;p++)
    {
      if(model.getparam(p).getallowtovary()) nvaryingparams++;
    }

    if(!analmargprec)
    {
      // nparams now plus one to include precision:
      parammeans.ReSize(nparams+1);
    }
    else
    {
      parammeans.ReSize(nparams);
    }

    parammeans = 0;

    // initialize param values to init values
    for(int p=1; p<=nparams;p++)
    {
      parammeans(p)  = model.getparam(p-1).getinitvalue();
    }

    if(!analmargprec)
    {
      float errorprecision = 0.0;

      if(prec<0)
	  {
	    updateprec = true;
	    ColumnVector r = data-model.nonlinearfunc(parammeans);

	    if(updateprec)
        {
          errorprecision = ntpts/SumSquare(r);
        }

	    float var = Sqr(errorprecision)*1000000;
	    float a = Sqr(errorprecision)/var;
	    float b = errorprecision/var;

	    GammaPrior tmpgamprior = GammaPrior(a,b);
	    model.add_param("prec",errorprecision,errorprecision/10.0,tmpgamprior);
	    // set precision in parammeans
	    parammeans(nparams+1) = model.getparam(nparams).getinitvalue();
	    nparams =  model.getnparams();
	  }
      else
	  {
	    updateprec = false;
	    errorprecision = prec;
	    // remove precision in parammeans
	    parammeans = parammeans.Rows(1,nparams);
	  }
    }

  }

  void LSLaplaceVoxelManager::run()
  {
    Tracer_Plus trace("LSLaplaceVoxelManager::run");

    if(debuglevel==2)
    {
      cout << parammeans.t() << endl;
      cout << evalfunction->evaluate(parammeans) << endl;
    }

    ColumnVector paramstovaryflags(parammeans.Nrows());
    for(int p=0;p<nparams;p++)
    {
      paramstovaryflags(p+1) = model.getparam(p).getallowtovary();
    }

    evalfunction->minimize(parammeans,paramstovaryflags);

    if(debuglevel==2)
    {
      cout << parammeans.t() << endl;
      cout << evalfunction->evaluate(parammeans) << endl;
    }

    bool finished = false;

    int power=-10;
    while(!finished && power<10)
    {
      paraminvcovs = hessian(parammeans, *evalfunction, std::pow(double(10.0),double(power)), 4);
      finished = true;

      for(int p=0;p<nparams;p++)
        if(paramstovaryflags(p+1))
          if(paraminvcovs(p+1,p+1) == 0)
	      {
            finished = false;
            power++;
            break;
	      }
    }

    if(debuglevel==2)
    {
      cout << power << endl;
      cout << paraminvcovs << endl;
    }

    // prune out non varying parameters:
    SymmetricMatrix paraminvcovstmp = paraminvcovs;
    paraminvcovstmp = 0;
    int vp = 0;
    for(int p=0;p<nparams;p++)
      if(paramstovaryflags(p+1))
      {
        vp++;
        paraminvcovstmp(vp,vp)=paraminvcovs(p+1,p+1);
        // for(int q=0;q<nparams;q++)
        // 	    if(paramstovaryflags(q+1))
        // 	      paraminvcovstmp(vp,q+1)=paraminvcovs(p+1,q+1);
      }

    paraminvcovs = paraminvcovstmp.SymSubMatrix(1,vp);

    if(power > 9)
    {
      cout << "Second derivative zero in hessian calculation" << endl;
      paraminvcovs << IdentityMatrix(nparams);
      //throw Exception("Second derivative zero in hessian calculation");
    }

  }

}
